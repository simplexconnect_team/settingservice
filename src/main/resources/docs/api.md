# Settings Management Service API Documentation

##Get /categories

###Success Response

### 200 OK 

---javascript
{
     "statusCode": 200,
     "object": [
        {
            "id": 1,
            "branch": "Colombo"
        },
        {
            "id": 2,
            "branch": "Greater Colombo"
        }
    ]  
}

##Post /categories

###Request

--javascript
{
	"category":"Ambalangoda"
}

###success Response

###200 OK

--javascript
{	
    "message":"category created",
    "statusCode": 200,
    "object": [
        {
            "id": 3,
            "category":"Ambalangoda"
        }
        ]
}

###Error Response

### 409 conflict 

--javascript
{
	"message": "Category already exists",
    "error": "Conflict",
 	"statusCode": 409
}


##Get  /branches 

###Success Response

### 200 OK 

---javascript
{
     "statusCode": 200,
     "object": [
        {
            "id": 32,
            "branch": "Ambalangoda",
            "owner": null,
            "categories": [
            				{
            				 "id":3,
           					 "category":"Ambalangoda"
           					 }
           				  ]
        },
        {
            "id": 47,
            "branch": "Ampara",
            "owner": null,
            "categories": null
        }
    ]  
}


##Post /branches 

###Request

--javascript
{
	"branch":"Ambalangoda",
	"owner":"test",
	"categories":[{
            	   "id":3,
           		   "category":"Ambalangoda"
           		}]
}

###success Response

###200 OK

--javascript
{	
    "message":"branch created",
    "statusCode": 200,
    "object": [
        {
            "id": 47,
            "branch":"Ambalangoda",
			"owner":"test",
        	"categories":["category1","category2"]
        }
        ]
}

###Error Response

### 409 conflict 

--javascript
{
	"message": "Branch already exists",
    "error": "Conflict",
 	"statusCode": 409
}

##Post /branches/{id}/categories

###Request

--javascript
[
 {
  "id":31,
  "category":"Ambalangoda"
  },
  {
  "id":51,
  "category":"Ampara"
  }
]

###success response

###200 OK

--javascript
{
    "message": "Categories Assign to Branch",
    "statusCode": 200,
    "object": {
        "id": 59,
        "branch": "Ambalangoda1",
        "owner": "test",
        "categories": [
            {
                "id": 51,
                "category": "Ampara"
            }
        ]
    }
}


##Get /regions

###success response
###200 ok 

{
    "statusCode": 200,
    "object": [
        {
            "id": 1,
            "region": "Central",
            "owner": "Ushanthi Vithanage",
            "branches": []
        },
        {
            "id": 2,
            "region": "West",
            "owner": "Balakrishnan Sundar",
            "branches": []
        },	
		]
}


##Post /regions

###Request

--javascript
{
	"region":"west"
}

###success response 

###200 OK status

 --javascript
 
  {
    "message": "Region Created",
    "statusCode": 200,
    "object": {
        "id": 11,
        "region": "ampara33",
        "owner": "anuthiranK",
        "branches": null
    }
  }

###Error Response

### 409 conflict 

--javascript
{
	"message": "Region already exists",
    "error": "Conflict",
 	"statusCode": 409
}


## Post  /regions/{id}/branches

-- 
###Request

--javascript
{
	[
	 {
	 "id":41
	 },
	 {
	 "id":43
	 }
    ]
}

### success response

### 200 OK Status

--javascript
{
    "message": "Branches Assign to Region",
    "statusCode": 200,
    "object": {
        "id": 5,
        "region": "ampara",
        "owner": "anuthiran",
        "branches": [
            {
                "id": 41,
                "branch": "Kegalle",
                "owner": null,
                "categories": [
                    {
                        "id": 40,
                        "category": "Kegalle"
                    }
                ]
            },
            {
                "id": 43,
                "branch": "Anuradhapura",
                "owner": null,
                "categories": [
                    {
                        "id": 43,
                        "category": "Anuradhapura"
                    }
                ]
            }
        ]
    }
}
  




  
  
  
  











































