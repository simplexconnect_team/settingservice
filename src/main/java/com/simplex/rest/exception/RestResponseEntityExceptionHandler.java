package com.simplex.rest.exception;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;
import com.simplex.response.util.GenericResponse;
import com.simplex.setting.error.ResourceAlreadyExistsException;

@ControllerAdvice
public class RestResponseEntityExceptionHandler extends ResponseEntityExceptionHandler {
	
	private Logger logger = LoggerFactory.getLogger(RestResponseEntityExceptionHandler.class);
    
    @ExceptionHandler({ResourceAlreadyExistsException.class})
    public ResponseEntity<Object> handleAlreadyExist(RuntimeException ex, WebRequest request) {
        logger.error("Conflict Resource", ex.getLocalizedMessage());
        GenericResponse response = new GenericResponse(ex.getMessage(),"Conflict",HttpStatus.CONFLICT.value());
        return new ResponseEntity<Object>(response, HttpStatus.CONFLICT);
    }
   
}