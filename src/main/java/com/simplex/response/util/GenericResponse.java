package com.simplex.response.util;

import java.util.List;

import org.springframework.validation.FieldError;
import org.springframework.validation.ObjectError;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class GenericResponse {

	private String message;
	private String error;
	private int statusCode;
	private Object object;
	private List<Object> objectList;

	public GenericResponse(List<Object> objectList, int statusCode) {
		super();
		this.objectList = objectList;
		this.statusCode = statusCode;
	}

	public GenericResponse(String message, int statusCode) {
		super();
		this.message = message;
		this.statusCode = statusCode;
	}

	public GenericResponse(Object object, int statusCode) {
		super();
		this.object = object;
		this.statusCode = statusCode;
	}

	public GenericResponse(String message, Object object, int statusCode) {
		super();
		this.message = message;
		this.object = object;
		this.statusCode = statusCode;
	}

	public GenericResponse(String message, List<Object> objectList, int statusCode) {
		super();
		this.message = message;
		this.objectList = objectList;
		this.statusCode = statusCode;
	}

	public GenericResponse(String message, String error, int statusCode) {
		super();
		this.message = message;
		this.error = error;
		this.statusCode = statusCode;
	}

	public GenericResponse(List<FieldError> fieldErrors, List<ObjectError> globalErrors) {
		super();
		final ObjectMapper mapper = new ObjectMapper();
		try {
			this.message = mapper.writeValueAsString(fieldErrors);
			this.error = mapper.writeValueAsString(globalErrors);
		} catch (final JsonProcessingException e) {
			this.message = "";
			this.error = "";
		}
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public String getError() {
		return error;
	}

	public void setError(String error) {
		this.error = error;
	}

	public int getStatusCode() {
		return statusCode;
	}

	public void setStatusCode(int statusCode) {
		this.statusCode = statusCode;
	}

	public Object getObject() {
		return object;
	}

	public void setObject(Object object) {
		this.object = object;
	}

	public List<Object> getObjectList() {
		return objectList;
	}

	public void setObjectList(List<Object> objectList) {
		this.objectList = objectList;
	}

}
