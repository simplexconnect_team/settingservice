package com.simplex.setting.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import com.simplex.setting.model.Branch;

public interface BranchRepository extends JpaRepository<Branch,Long>{

	@Query("SELECT new Branch(id, branch) "
	    + "FROM Branch "
	    + "ORDER BY branch")
	List<Branch> findAllBranches();	
	
}