package com.simplex.setting.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.simplex.setting.model.Category;

public interface CategoryRepository extends JpaRepository<Category,Long> {
	
	@SuppressWarnings("unchecked")
	Category save(Category category);
                                      
	List<Category> findAll();
		
}