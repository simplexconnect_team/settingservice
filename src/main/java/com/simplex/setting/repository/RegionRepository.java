package com.simplex.setting.repository;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.simplex.setting.model.Region;

public interface RegionRepository extends JpaRepository<Region,Long> {
	
	List<Region> findAll();

	@SuppressWarnings("unchecked")
	Region save(Region region);
	
}