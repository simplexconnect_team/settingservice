package com.simplex.setting.service;
import java.util.List;

import com.simplex.setting.model.Branch;
import com.simplex.setting.model.Category;

public interface BranchDataService {
	
	List<Branch> getAllBranches();
	
	Branch save(Branch branch);
	
	Branch assignCategories(List<Category> categoryList,Long id);

}
