package com.simplex.setting.service;

import java.util.List;

import com.simplex.setting.model.Branch;
import com.simplex.setting.model.Region;

public interface RegionService {
	
	List<Region> findAll();
	
	Region save(Region region);
	
	Region assignBranches(Long regionId,List<Branch> branches);

}
