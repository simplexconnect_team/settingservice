package com.simplex.setting.service;

import java.util.List;

import com.simplex.setting.model.Category;

public interface CategoryService {

	Category save(Category category);
	 									
	List<Category> findAllCategories();
	
	
}
