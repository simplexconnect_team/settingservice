package com.simplex.setting.service;

import java.util.List;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.simplex.setting.model.Branch;
import com.simplex.setting.model.Category;
import com.simplex.setting.repository.BranchRepository;

@Service("branchDataService")
public class BranchDataServiceImpl implements BranchDataService {
	
	@Autowired
	BranchRepository branchRepository;

	@Override
	public List<Branch> getAllBranches() {
		return branchRepository.findAllBranches();
	}

	@Override
	public Branch save(Branch branch) {
		return branchRepository.save(branch);
	}

	@Override	
	public Branch assignCategories(List<Category> categoryList,Long id) {
		Optional<Branch> exisingBranch = branchRepository.findById(id);
		exisingBranch.get().setCategories(categoryList);
		return branchRepository.save(exisingBranch.get());
	}

}
