package com.simplex.setting.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.simplex.setting.model.Branch;
import com.simplex.setting.model.Region;
import com.simplex.setting.repository.RegionRepository;

@Service("RegionService")
public class RegionServiceImpl implements RegionService {

	@Autowired
	RegionRepository regionRepository;
	
	@Override
	public List<Region> findAll() {		
		return regionRepository.findAll();
	}
	@Override
	public Region save(Region region) {
		
		return regionRepository.save(region);
	}
	@Override
	public Region assignBranches(Long regionId, List<Branch> branches) {
		Optional<Region> region = regionRepository.findById(regionId);
		region.get().setBranches(branches);
		return regionRepository.save(region.get());
	}

}
