package com.simplex.setting.model;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name="region")
public class Region {
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name="regionId")
	private Long id;
	private String region;
	private String owner;
	@OneToMany(fetch=FetchType.EAGER)
    @JoinTable(
        name="region_branch", 
        joinColumns=@JoinColumn(name="regionId"), 
        inverseJoinColumns=@JoinColumn(name="branchId"))
	private List<Branch> branches;
	
	public Region(){
		super();
	}
	
	public Region(Long id){
		super();
		this.id = id;
	}
	public Region(Long id, String region){
		super();
		this.id = id;
		this.region = region;
	}
	public Region(Long id, String region, String owner){
		super();
		this.id = id;
		this.region = region;
		this.owner = owner;
	}
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	
	public String getRegion() {
		return region;
	}
	public void setRegion(String region) {
		this.region = region;
	}
			
	public List<Branch> getBranches() {
		return branches;
	}
	public void setBranches(List<Branch> branches) {
		this.branches = branches;
	}
	
	public String getOwner() {
		return owner;
	}
	public void setOwner(String owner) {
		this.owner = owner;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Region other = (Region) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}
	
	@Override
	public String toString() {
		return "Region [id=" + id + ", region=" + region + "]";
	}

}