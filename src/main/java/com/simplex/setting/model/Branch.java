package com.simplex.setting.model;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name="branch")
public class Branch implements Serializable, Comparable<Branch> {
	
	private static final long serialVersionUID = -6639352617019258726L;
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name="branchId")
	private Long id;
	private String branch;
	private String owner;
	@OneToMany(fetch=FetchType.EAGER)
    @JoinTable(
        name="branch_category", 
        joinColumns=@JoinColumn(name="branchId"), 
        inverseJoinColumns=@JoinColumn(name="categoryId"))
	private List<Category> categories;
	
	public Branch(){
		super();
	}
	public Branch(Long id){
		super();
		this.id = id;
	}
	public Branch(Long id, String branch){
		super();
		this.id = id;
		this.branch = branch;
	}
	public Branch(Long id, String branch, String owner) {
		super();
		this.id = id;
		this.branch = branch;
		this.owner = owner;
	}
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	
	public String getBranch() {
		return branch;
	}
	public void setBranch(String branch) {
		this.branch = branch;
	}
		
	public List<Category> getCategories() {
		return categories;
	}
	public void setCategories(List<Category> categories) {
		this.categories = categories;
	}
	
	public String getOwner() {
		return owner;
	}
	public void setOwner(String owner) {
		this.owner = owner;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Branch other = (Branch) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}
	
	@Override
	public String toString() {
		return "Branch [id=" + id + ", branch=" + branch + "]";
	}
	
	@Override
	public int compareTo(Branch b) {
		return this.getBranch().compareTo(b.getBranch());
	}
		
}