package com.simplex.setting.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.simplex.response.util.GenericResponse;
import com.simplex.setting.error.ResourceAlreadyExistsException;
import com.simplex.setting.model.Branch;
import com.simplex.setting.model.Category;
import com.simplex.setting.service.BranchDataService;

@RestController
@RequestMapping("/branches")
public class BranchController {
	
	@Autowired
	BranchDataService branchDataService;
		
	@GetMapping
	public ResponseEntity<?> getAllBranches(){
		GenericResponse genericResponse = new GenericResponse(branchDataService.getAllBranches(),HttpStatus.OK.value());
		return new ResponseEntity<GenericResponse>(genericResponse,HttpStatus.OK);
	}
	
	@PostMapping
	public ResponseEntity<?> saveBranch(@RequestBody Branch branch){
		GenericResponse response;
		try {
			response = new GenericResponse("Branch Created",branchDataService.save(branch), HttpStatus.OK.value());
		} catch (DataIntegrityViolationException ex) {
			throw new ResourceAlreadyExistsException("Branch Already Exists");
		}
			return new ResponseEntity<GenericResponse>(response, HttpStatus.OK);
		}
	
	@PostMapping("/{id}/categories")
	public ResponseEntity<?> assignCategories(@RequestBody List<Category> categoryList,@PathVariable("id") Long id){
		System.out.println("Id: "+id);
		GenericResponse response = new GenericResponse("Categories Assign to Branch",branchDataService.assignCategories(categoryList,id), HttpStatus.OK.value());
		return new ResponseEntity<GenericResponse>(response, HttpStatus.OK);
	}
	
}
