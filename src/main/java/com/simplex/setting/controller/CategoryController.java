package com.simplex.setting.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.simplex.response.util.GenericResponse;
import com.simplex.setting.error.ResourceAlreadyExistsException;
import com.simplex.setting.model.Category;
import com.simplex.setting.service.CategoryService;

@RestController
@RequestMapping("/categories")
public class CategoryController {
	
	@Autowired
	CategoryService categoryService;

	
	@PostMapping
	public ResponseEntity<?> saveCategory(@RequestBody Category category) throws Exception{
		GenericResponse response;
		try {
			response = new GenericResponse("Category Created",categoryService.save(category), HttpStatus.OK.value());
		} catch (DataIntegrityViolationException ex) {
			throw new ResourceAlreadyExistsException("Category Already Exists");
		}
			return new ResponseEntity<GenericResponse>(response, HttpStatus.OK);
	} 
	
	@GetMapping
	public ResponseEntity<?> findAllCategories(){
		GenericResponse response = new GenericResponse(categoryService.findAllCategories(),HttpStatus.OK.value());
		return new ResponseEntity<GenericResponse>(response,HttpStatus.OK);
	}
	
	
	
	
	
}
