package com.simplex.setting.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.simplex.response.util.GenericResponse;
import com.simplex.setting.error.ResourceAlreadyExistsException;
import com.simplex.setting.model.Branch;
import com.simplex.setting.model.Region;
import com.simplex.setting.service.RegionService;

@RestController
@RequestMapping("/regions")
public class RegionController {
	
	@Autowired
	RegionService regionService;
	
	@GetMapping
	public ResponseEntity<?> findAllRegions(){
		GenericResponse response = new GenericResponse(regionService.findAll(),HttpStatus.OK.value());
		return new ResponseEntity<GenericResponse>(response,HttpStatus.OK);
	}
	
	@PostMapping
	public ResponseEntity<?> saveRegion(@RequestBody Region region){
		GenericResponse response;
		try {
			response = new GenericResponse("Region Created",regionService.save(region),HttpStatus.OK.value());
		} catch (DataIntegrityViolationException ex) {
			throw new ResourceAlreadyExistsException("Region Already Exists");
		}	
		return new ResponseEntity<GenericResponse>(response,HttpStatus.OK);		
	}
	
	@PostMapping("/{id}/branches")
	public ResponseEntity<?> assignBranches(@RequestBody List<Branch> branches,@PathVariable Long id){
		GenericResponse response= new GenericResponse("Branches Assign to Region", regionService.assignBranches(id, branches),HttpStatus.OK.value());
		return new ResponseEntity<GenericResponse>(response,HttpStatus.OK);		
	}
	
	
	
	
}